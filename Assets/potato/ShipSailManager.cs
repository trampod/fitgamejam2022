using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSailManager : MonoBehaviour
{
    [SerializeField]
    public MastManager ForeMast;
    [SerializeField]
    public MastManager MainMast;
    [SerializeField]
    public MastManager MizzenMast;
    [SerializeField]
    public MastManager Bowsprit;
    
    public List<SkinnedMeshRenderer> ForeStaysails;
    public List<SkinnedMeshRenderer> AftStaysails;    

    [SerializeField]
    public List<MastManager> DestructionQueue;
    private int hitCount = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        SetSpeed0();
    }

    private void enableStaysails(List<SkinnedMeshRenderer> sails, bool val)
    {
        foreach (SkinnedMeshRenderer rend in sails)
        {
            rend.enabled = val;
        }
    }
    private void TrySetStaysails()
    {
        if(!Bowsprit.isMastDamaged() && !ForeMast.isMastDamaged())
            enableStaysails(ForeStaysails, true);
        else
            enableStaysails(ForeStaysails, false);
        if(!MainMast.isMastDamaged() && !ForeMast.isMastDamaged())
            enableStaysails(AftStaysails, true);
        else
            enableStaysails(AftStaysails, false);
    }

    private void SetSpeed0()
    {
        ForeMast.SetSpeed0();
        MainMast.SetSpeed0();
        MizzenMast.SetSpeed0();
        enableStaysails(ForeStaysails, false);
        enableStaysails(AftStaysails, false);
    }

    private void SetSpeed1()
    {
        ForeMast.SetSpeed1();
        MainMast.SetSpeed1();
        MizzenMast.SetSpeed1();
        TrySetStaysails();
    }

    private void SetSpeed2()
    {
        ForeMast.SetSpeed2();
        MainMast.SetSpeed2();
        MizzenMast.SetSpeed2();
        TrySetStaysails();
    }

    private void SetSpeed3()
    {
        ForeMast.SetSpeed3();
        MainMast.SetSpeed3();
        MizzenMast.SetSpeed3();
        TrySetStaysails();
    }

    private void DisconnectPart(GameObject part)
    {        
        part.transform.parent = null;
        var body = part.AddComponent<Rigidbody>();
        body.AddForce(Vector3.up * 10, ForceMode.Impulse);
        body.AddForce(Vector3.left * 2 * UnityEngine.Random.Range(-1, 2), ForceMode.Impulse);
    }

    public void TakeDamage()
    {
        if(DestructionQueue.Count > hitCount)
        {
            MastManager next = DestructionQueue[hitCount++];
            if(next.breakawayPart)
            {
                DisconnectPart(next.breakawayPart);
                next.damageMast();
            }
        }
    }

    public void SetSpeed(Movement.SpeedMode speedMode)
    {
        switch (speedMode)
        {
            case Movement.SpeedMode.Still:
                SetSpeed0();
                break;
            case Movement.SpeedMode.Slow:
                SetSpeed1();
                break;
            case Movement.SpeedMode.Faster:
                SetSpeed2();
                break;
            case Movement.SpeedMode.Fast:
                SetSpeed3();
                break;
        }
        
    }
}
