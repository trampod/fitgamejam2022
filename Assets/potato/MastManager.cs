using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MastManager : MonoBehaviour
{
    bool mastDamaged = false;

    public GameObject breakawayPart;

    public SkinnedMeshRenderer mainsail;
    public MeshRenderer mainsailPacked;

    public SkinnedMeshRenderer topsail;
    public MeshRenderer topsailPacked;

    public SkinnedMeshRenderer topgallant;
    public MeshRenderer topgallantPacked;

    public SkinnedMeshRenderer royal;
    public MeshRenderer royalPacked;

    private void SetSail(SkinnedMeshRenderer sail, MeshRenderer packed)
    {   if(sail)
            sail.enabled = true;
        if(packed)    
        packed.enabled = false;
    }
    private void PackSail(SkinnedMeshRenderer sail, MeshRenderer packed)
    {
        if(sail)
            sail.enabled = false;
        if(packed)
            packed.enabled = true;
    }

    public void damageMast()
    {
        mastDamaged = true;
    }

    public bool isMastDamaged()
    {
        return mastDamaged;
    }

    public void SetSpeed0()
    {
        PackSail(mainsail, mainsailPacked);

        if(!mastDamaged)
        {
            PackSail(topsail, topsailPacked);
            PackSail(topgallant, topgallantPacked);
            PackSail(royal, royalPacked);
        }
    }
    public void SetSpeed1()
    {
        SetSail(mainsail, mainsailPacked);

        if(!mastDamaged)
        {
            PackSail(topsail, topsailPacked);
            PackSail(topgallant, topgallantPacked);
            PackSail(royal, royalPacked);
        }
    }

    public void SetSpeed2()
    {
        SetSail(mainsail, mainsailPacked);

        if(!mastDamaged)
        {
            SetSail(topsail, topsailPacked);
            PackSail(topgallant, topgallantPacked);
            PackSail(royal, royalPacked);
        }
    }

    public void SetSpeed3()
    {
        SetSail(mainsail, mainsailPacked);

        if(!mastDamaged)
        {
            SetSail(topsail, topsailPacked);
            SetSail(topgallant, topgallantPacked);
            SetSail(royal, royalPacked);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //SetSpeed1();
    }
}
