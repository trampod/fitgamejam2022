using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShatterableObject : MonoBehaviour
{
    public MeshRenderer mainMeshRenderer;
    public List<GameObject> ShatteredParts;
    // Start is called before the first frame update
    void Start()
    {
        mainMeshRenderer = GetComponent<MeshRenderer>();
    }

    public void Shatter()
    {
        mainMeshRenderer.enabled = false;
        foreach (GameObject part in ShatteredParts)
        {
            part.SetActive(true);
        }
    }

}
