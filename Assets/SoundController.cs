using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioSource ambient;
    public AudioSource crash;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void PlayBackgroundMusic()
    {
        ambient.Play();
    }
    public void StopBackgroundMusic()
    {
        ambient.Stop();
    }
    public void PlayHitEffect()
    {
        crash.Play();
    }
}
