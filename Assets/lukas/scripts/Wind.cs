using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : MonoBehaviour
{
    public enum Direction
    {
        Left,
        Right,
        Forwards,
        Backwards
    }

    void Start()
    {
        WindVisualAnchor.transform.rotation = Quaternion.LookRotation(windDirections[windDirection]);
        WindVisualAnchor.transform.Rotate(0,-90f,0);
    }

    public GameObject WindVisualAnchor;
    public ParticleSystem windVisual;
    public Direction windDirection = Direction.Left;
    public float windSpeed = 2f;
    
    private readonly Dictionary<Direction, Vector3> windDirections = new Dictionary<Direction, Vector3>
    {
        { Direction.Left, Vector3.left },
        { Direction.Right, Vector3.right },
        { Direction.Forwards, Vector3.forward },
        { Direction.Backwards, Vector3.back }
    };

    public Vector3 WindVector()
    {
        return windDirections[windDirection] * windSpeed;
    }
}
