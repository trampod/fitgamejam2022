using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public TextMeshProUGUI textSinking;
    public TextMeshProUGUI textDropCargo;

    public GameObject boxPrefab;
    
    public float timer = 25;
    public float timeGain = 2;
    public int lives = 3;
    public int cargoBoxes = 5;
    
    private bool sinking;

    private UI ui;
    private Ship ship;
    private Level level;
    // Start is called before the first frame update
    void Start()
    {
        ship = GetComponentInParent<Ship>();
        ui = GetComponentInParent<UI>();
        level = GetComponentInParent<Level>();
        
        ui.InitializeBoxes(cargoBoxes);
    }

    public void DealDamage()
    {
        lives--;

        sinking = lives <= 0;

        if (sinking) {
            textDropCargo.enabled = true;
            textSinking.enabled = true;
        }

        ship.movement.SlowDown(true);
        ship.movement.LowerMaxSpeed();
    }

    public bool Sunk()
    {
        return timer < 0;
    }

    public int LivesRemaining()
    {
        return lives;
    }
    
    public int CargoBoxesRemaining()
    {
        return cargoBoxes;
    }

    private void Update()
    {
        if (Sunk())
            level.SetGameOver();

        if (sinking) {
            timer -= Time.deltaTime;

            var time = TimeSpan.Zero;
            if (timer >= 0) {
                time = TimeSpan.FromSeconds(timer);
            }
            textSinking.text = $"SINKING {time.ToString(@"mm\:ss\.fff")}";
        }

        if (level.IsGameOver())
            return;

        if (sinking && cargoBoxes > 0) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                ui.DropBox();
                
                cargoBoxes--;
                timer += timeGain;

                var box = Instantiate(boxPrefab);

                var shipTransform = ship.transform;
                box.transform.position = shipTransform.position + shipTransform.forward * -4f + shipTransform.right * 2f;
            }
        }
    }
}
