using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour
{
    
    private TriggerCollisionInvoker rockCollisionInvoker;

    private new Collider collider;

    // Start is called before the first frame update
    void Start()
    {
        collider = GetComponent<Collider>();
        
        rockCollisionInvoker = new TriggerCollisionInvoker
        {
            { "Ship", OnShipCollision }
        };
    }

    private void OnTriggerStay(Collider other)
    {
        rockCollisionInvoker.InvokeCollision(other);
    }

    private void OnShipCollision(Collider other)
    {
        var health = other.gameObject.GetComponentInChildren<Health>();
        var manager = other.gameObject.GetComponent<ShipSailManager>();
        
        manager.TakeDamage();
        health.DealDamage();
        

        collider.enabled = false;
        ShatterableObject ShatterObj;
        if (TryGetComponent<ShatterableObject>(out ShatterObj))
            ShatterObj.Shatter();
    }
}
