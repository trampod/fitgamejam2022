using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public RectTransform boxesTransform;
    public GameObject box;
    public Image sailMode;

    public List<Sprite> modes = new List<Sprite>();

    private float imageHeight = 70;

    private GameObject[] boxes;
    private int currentBoxIndex;

    private void Start()
    {
        imageHeight = box.GetComponent<RectTransform>().rect.height;
    }

    public void InitializeBoxes(int count)
    {
        boxes = new GameObject[count];
        currentBoxIndex = count-1;

        for (int i = 0 ; i < count ; i++) {
            var newBox = Instantiate(box, boxesTransform);
            boxes[i] = newBox;

            newBox.name = $"boxImage_{i+1}";
            newBox.transform.position = boxesTransform.position + Vector3.up * (imageHeight * i / 2);
        }
    }

    public void DropBox()
    {
        boxes[currentBoxIndex].GetComponent<Image>().enabled = false;
        currentBoxIndex--;
    }
    
    public void UpdateFlagMode(Movement.SpeedMode speedMode)
    {
        sailMode.sprite = modes[(int)speedMode];
    }
}
