using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bank : MonoBehaviour
{
    private TriggerCollisionInvoker bankCollisionInvoker;

    // Start is called before the first frame update
    void Start()
    {
        bankCollisionInvoker = new TriggerCollisionInvoker
        {
            { "Ship", OnShipCollision }
        };
    }

    private void OnTriggerEnter(Collider other)
    {
        bankCollisionInvoker.InvokeCollision(other);
    }

    private void OnShipCollision(Collider other)
    {
        var ship = other.gameObject.GetComponent<Ship>();

        ship.movement.Stop();
    }
}
