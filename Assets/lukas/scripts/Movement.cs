﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private void Start()
    {
        ui = GetComponentInParent<UI>();
        manager = GetComponentInParent<ShipSailManager>();
        speed = speedModes[currentSpeedMode].speed;
    }

    private UI ui;
    private ShipSailManager manager;
    
    private DateTime lastSpeedChange = DateTime.Now.AddSeconds(-2);
    private SpeedMode currentSpeedMode = SpeedMode.Still;
    private SpeedMode maxSpeedMode = SpeedMode.Fast;
    private const SpeedMode minSpeedMode = SpeedMode.Slow;
    
    public float speed;
    public float bleed = 8f;

    public enum SpeedMode
    {
        Still = 0,
        Slow = 1,
        Faster = 2,
        Fast = 3
    }
    
    struct Behaviour
    {
        public float speed;
        public float radius;
    }
    
    private readonly Dictionary<SpeedMode, Behaviour> speedModes = new Dictionary<SpeedMode, Behaviour>
    {
        { SpeedMode.Still, new Behaviour{ speed = 0f, radius = 0f } },
        { SpeedMode.Slow, new Behaviour{ speed = 8f, radius = 4f } },
        { SpeedMode.Faster, new Behaviour{ speed = 16f, radius = 6f } },
        { SpeedMode.Fast, new Behaviour{ speed = 24f, radius = 9f } }
    };

    private bool CanChangeSpeed()
    {
        return DateTime.Now.Subtract(lastSpeedChange) > TimeSpan.FromSeconds(.5f);
    }

    public float GetSpeed()
    {
        if (speed < speedModes[currentSpeedMode].speed) {
            speed += bleed * Time.deltaTime;
        }
        if (speed > speedModes[currentSpeedMode].speed) {
            speed -= bleed * Time.deltaTime;
        }
        return speed;
    }

    public float GetRadius()
    {
        return speedModes[currentSpeedMode].radius;
    }

    public void LowerMaxSpeed()
    {
        if (maxSpeedMode - 1 < minSpeedMode)
            return;
        maxSpeedMode -= 1;
    }

    public bool SpeedUp(bool ignoreTimeout = false)
    {
        if (currentSpeedMode >= maxSpeedMode)
            return false;

        if (!CanChangeSpeed() && !ignoreTimeout)
            return false;
        
        currentSpeedMode += 1;

        manager.SetSpeed(currentSpeedMode);
        ui.UpdateFlagMode(currentSpeedMode);

        lastSpeedChange = DateTime.Now;
        
        return true;
    }

    public bool SlowDown(bool ignoreTimeout = false)
    {
        if (currentSpeedMode <= SpeedMode.Still)
            return false;
        
        if (!CanChangeSpeed() && !ignoreTimeout)
            return false;

        currentSpeedMode -= 1;

        manager.SetSpeed(currentSpeedMode);
        ui.UpdateFlagMode(currentSpeedMode);

        lastSpeedChange = DateTime.Now;

        return true;
    }

    public void Stop()
    {
        currentSpeedMode = SpeedMode.Still;
        speed = 0;
        lastSpeedChange = DateTime.Now;
    }

    public bool Stopped()
    {
        return currentSpeedMode == SpeedMode.Still;
    }

    public SpeedMode CurrentSpeedMode()
    {
        return currentSpeedMode;
    }
    
    public SpeedMode MaxSpeedMode()
    {
        return maxSpeedMode;
    }
}