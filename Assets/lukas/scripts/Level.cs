using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Level : MonoBehaviour
{
    public SoundController sound;
    public TextMeshProUGUI levelTimer;
    public Canvas debugUI;
    public TextMeshProUGUI textGameOver;
    public TextMeshProUGUI textStartGame;
    public TextMeshProUGUI textGameWin;
    public TextMeshProUGUI textScore;


    public float levelTime = 100;
    public bool debug;
    
    private bool gameOver;


    private bool paused = true;
    private float fov = 60;
    
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = GetComponentInChildren<Camera>();
        
        if (debug) {
            debugUI.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        TimeSpan time = TimeSpan.FromSeconds(levelTime);
        
        if (levelTime < 0)
            time = TimeSpan.Zero;
        
        levelTimer.text = time.ToString(@"mm\:ss\.fff");
        
        if (paused) {
            if (Input.GetKeyDown(KeyCode.W)) {
                if(sound)
                    sound.PlayBackgroundMusic();
                paused = false;
            }
        }

        if (paused) {
            Time.timeScale = 0;
            return;
        }

        if (mainCamera.fieldOfView > fov)
            mainCamera.fieldOfView -= Time.deltaTime * 10;

        levelTimer.enabled = true;
        textStartGame.enabled = false;
        
        Time.timeScale = 1;

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        
        if (gameOver)
            return;

        

        levelTime -= Time.deltaTime;

        if (levelTime < 0) {
            textGameOver.enabled = true;
            gameOver = true;
        }
    }

    public void SetScore(int score)
    {
        textScore.text = $"Score: {score.ToString("0000")}";
        textScore.enabled = true;
    }

    public void Finnish()
    {
        gameOver = true;
        textGameWin.enabled = true;
    }

    public int TimeRemaining()
    {
        return (int)(levelTime * 100);
    }
    
    public void SetGameOver()
    {
        textGameOver.enabled = true;
        gameOver = true;
    }

    public bool IsGameOver()
    {
        return gameOver;
    }

    public bool Debug()
    {
        return debug;
    }
}
