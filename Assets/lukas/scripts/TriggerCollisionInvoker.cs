﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TriggerCollisionInvoker: Dictionary<string, Action<Collider>>
{
    public void InvokeCollision(Collider collider)
    {
        var isLayer = new Func<string, bool>(layer => LayerMask.NameToLayer(layer) == collider.gameObject.layer);

        foreach (var action in this)
        {
            if (isLayer(action.Key))
            {
                action.Value(collider);
            }
        } 
    }
}