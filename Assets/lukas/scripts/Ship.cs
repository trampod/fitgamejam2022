using System;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public SoundController sound;
    public Transform rayCastOrigin;

    public Movement movement;

    private ShipSailManager manager;
    private Level level;
    private Health health;

    private Transform shipTransform;
    private Vector3 windVector;

    private TriggerCollisionInvoker enterCollisionInvoker;
    private TriggerCollisionInvoker exitCollisionInvoker;
    
    private bool onBank;

    // Start is called before the first frame update
    void Start()
    {
        shipTransform = GetComponent<Transform>();
        health = GetComponentInChildren<Health>();
        level = GetComponentInParent<Level>();
        manager = GetComponentInChildren<ShipSailManager>();
        movement = GetComponentInChildren<Movement>();


        enterCollisionInvoker = new TriggerCollisionInvoker
        {
            { "Wind", OnWindCollisionEnter },
            { "Bank", OnBankCollisionEnter },
            { "Hazard", OnHazardCollisionEnter }
        };
        
        exitCollisionInvoker = new TriggerCollisionInvoker
        {
            { "Wind", OnWindCollisionExit },
            { "Bank", OnBankCollisionExit }
        };
    }

    private void OnTriggerEnter(Collider other)
    {
        enterCollisionInvoker.InvokeCollision(other);
    }
    
    private void OnTriggerExit(Collider other)
    {
        exitCollisionInvoker.InvokeCollision(other);
    }

    void OnWindCollisionEnter(Collider other)
    {
        var wind = other.gameObject.GetComponent<Wind>();

        windVector = wind.WindVector();
    }
    
    void OnWindCollisionExit(Collider other)
    {
        windVector = new Vector3();
    }

    void OnHazardCollisionEnter(Collider other)
    {
        if(sound)
            sound.PlayHitEffect();
    }

    void OnBankCollisionEnter(Collider other)
    {
        onBank = true;
    }

    void OnBankCollisionExit(Collider other)
    {
        onBank = false;
    }
    
    void Update()
    {
        if (health.Sunk() || level.IsGameOver()) {
            return;
        }
        
        var canMove = true;
        
        var  ray = new Ray
        {
            origin = rayCastOrigin.position, 
            direction = Vector3.down
        };
        
        var hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Bank")) 
            {
                Debug.Log("bank hit");
                movement.Stop();
                canMove = false;
            }
        }

        Rotate();
        
        if (!canMove)
            return;

        MoveForward();
    }

    void MoveForward()
    {
        switch (true) {
            case var value when Input.GetKeyUp(KeyCode.W):
                movement.SpeedUp();
                break;
            case var value when Input.GetKeyUp(KeyCode.S):
                movement.SlowDown();
                break;
        }

        var moveVector = shipTransform.forward * movement.GetSpeed();
        if (!movement.Stopped() && !onBank)
        {
            moveVector += windVector;
        }

        shipTransform.position += moveVector * Time.deltaTime;
    }

    void Rotate()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");

        Vector3 offset = new Vector3();
        if (!movement.Stopped()) {
            offset = shipTransform.right * movement.GetRadius() * horizontalAxis;;
        }

        if (horizontalAxis != 0) {
            shipTransform.RotateAround(shipTransform.position + offset, Vector3.up, horizontalAxis * 30f * Time.deltaTime);
        }
    }
}

