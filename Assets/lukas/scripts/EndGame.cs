using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndGame : MonoBehaviour
{   
    public Health health;

    public int scorePerSecond = 1;
    public int scorePerLive = 2;
    public int scorePerCargoBox = 50;

    private Level level;
    private TriggerCollisionInvoker enterCollisionInvoker;

    // Start is called before the first frame update
    void Start()
    {
        level = GetComponentInParent<Level>();
        health = FindObjectOfType<Health>();

        enterCollisionInvoker = new TriggerCollisionInvoker
        {
            { "Ship", OnShipCollisionEnter }
        };
    }

    private void OnTriggerEnter(Collider other)
    {
        enterCollisionInvoker.InvokeCollision(other);
    }

    private void OnShipCollisionEnter(Collider other)
    {
        level.Finnish();

        var score = level.TimeRemaining() * scorePerSecond + health.LivesRemaining() * scorePerLive + health.CargoBoxesRemaining() * scorePerCargoBox;
        level.SetScore(score);    
    }
}
