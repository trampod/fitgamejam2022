using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSkip : MonoBehaviour
{
    public float SkipDistance;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Ship>() != null)
            this.transform.position = this.transform.position + Vector3.forward * SkipDistance;
    }
}
