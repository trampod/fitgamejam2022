using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenerateTrigger : MonoBehaviour
{
    private Action<Vector3> action;
    private Vector3 start;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Ship>() != null)
        {
            action(start);
            Destroy(this.gameObject);
        }
    }

    internal void SetAction(Action<Vector3> generateChunck, Vector3 vector3)
    {
        action = generateChunck;
        start = vector3;
    }
}
