using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//scale of ship object messed with flag rotation, so I made thiw workaround
public class Follow : MonoBehaviour
{
    public Transform FollowedObject;
    public Vector3 Offset;

    private void Update()
    {
        this.transform.position = (FollowedObject.position + Offset);
    }
}
