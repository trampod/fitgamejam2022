using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[Serializable]
public struct Obstacle
{
    public GameObject prefab;
}

public class RiverGeneration : MonoBehaviour
{
    
    public int LevelLength = 10;

    public int MinDistance = 1;
    public int MaxDistance = 3;

    public int ChunckSize = 20;
    private const int CHUNCK_WIDTH = 5;

    public int ChuncksBeforeSpecial = 1;
    private int _counter = 0;
    public int SpecialSize = 20;
    public int RestSpace = 4;
    public int XMin = -2;
    public int XMax = 2;

    public float BankDistance = 3;

    public Vector2 PositioningScale;

    public Vector2 PositioningOffset;

    public Vector2 PositioningDiviation;

    public int RockShare = 10;
    public int KrackenShare = 2;
    public int WhirlpoolShare = 5;

    public int TriggerDistance = 10;

    [SerializeField]
    public Wind WindPrefab;

    public List<int> windDirChanceDistr;
    public int WindIntervals = 6;
    public int WindLength = 5;
    public float MinWindSpeed = 0f;
    public float MaxWindSpeed = 3f;


    private char[,] currentChunck;

    public const char ROK = 'r';
    public const char WAT = ' ';
    public const char WIR = 'w';
    public const char KRA = 'k';

    public List<Obstacle> RockObstacles = new List<Obstacle>();
    public List<Obstacle> WirlpoolObstacles = new List<Obstacle>();
    public List<Obstacle> KrakenObstacles = new List<Obstacle>();
    public List<GameObject> LeftBank = new List<GameObject>();
    public List<GameObject> RightBank = new List<GameObject>();
    
    [SerializeField]
    public GameObject FinishPrefab;

    [SerializeField]
    public RegenerateTrigger TriggerPrefab;

    public List<GameObject> ChunckCache;
    public int MaxChunckCount = 5;
    public int BehindChuncks = 3;

    private int _chunkCount = 0;
    private int _currentChunck = 0;

    private int FarLook { get => MaxChunckCount - BehindChuncks; }


    private List<char[,,]> rockFormations = new List<char[,,]>
    {
        new char[,,]
        {
            {
                {ROK,WAT,ROK,WAT,ROK }
            },
            {
                {ROK,ROK,WAT,WAT,WAT }
            },
            {
                {ROK,ROK,ROK,WAT,WAT }
            },
            {
                {WAT,ROK,WAT,ROK,WAT }
            }
        },
        new char[,,]
        {
            {
                {ROK,ROK,WAT,WAT,WAT },
                {ROK,WAT,WAT,WAT,ROK }
            },
            {
                {ROK,WAT,WAT,WAT,ROK },
                {ROK,ROK,WAT,WAT,WAT }
            },
            {
                {ROK,WAT,ROK,WAT,WAT },
                {WAT,WAT,ROK,WAT,ROK }
            },
            {
                {ROK,WAT,WAT,WAT,ROK },
                {WAT,WAT,ROK,WAT,WAT }
            }
        },
        new char[,,]
        {
            {
                {ROK,WAT,WAT,WAT,ROK },
                {ROK,ROK,WAT,WAT,WAT },
                {ROK,WAT,WAT,WAT,ROK }
            },
            {
                {ROK,WAT,WAT,WAT,ROK },
                {ROK,WAT,ROK,WAT,ROK },
                {ROK,WAT,WAT,WAT,ROK }
            },
            {
                {WAT,WAT,ROK,WAT,WAT },
                {WAT,ROK,WAT,WAT,WAT },
                {ROK,WAT,WAT,WAT,ROK }
            },
            {
                {WAT,WAT,ROK,WAT,WAT },
                {WAT,WAT,WAT,WAT,ROK },
                {WAT,ROK,WAT,WAT,WAT }
            }
        },
        new char[,,]
        {
            {
                {ROK,WAT,WAT,WAT,WAT },
                {ROK,ROK,WAT,WAT,ROK },
                {ROK,WAT,WAT,WAT,ROK },
                {WAT,WAT,WAT,WAT,ROK }
            },
            {
                {WAT,WAT,ROK,WAT,WAT },
                {WAT,WAT,ROK,WAT,WAT },
                {WAT,WAT,ROK,WAT,WAT },
                {WAT,WAT,ROK,WAT,WAT }
            },
            {
                {ROK,ROK,WAT,WAT,WAT },
                {ROK,WAT,WAT,WAT,WAT },
                {WAT,WAT,WAT,WAT,ROK },
                {WAT,WAT,WAT,ROK,ROK }
            },
            {
                {WAT,WAT,WAT,ROK,WAT },
                {WAT,ROK,WAT,WAT,WAT },
                {WAT,WAT,WAT,ROK,WAT },
                {WAT,ROK,WAT,WAT,WAT }
            }
        }
    };

    private void Start()
    {
        ChunckCache = new List<GameObject>();
        //GenerateChunck(this.transform.position);
        GenerateStartPart(this.transform.position);
    }

    /*private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 150, 100), "Regenerate"))
        {
            foreach (Transform child in this.transform)
            {
                Destroy(child.gameObject);
            }
            GenerateChunck(this.transform.position);
        }
    }*/

    public void GenerateStartPart(Vector3 start)
    {
        Vector3 addition = new Vector3(0, 0, ChunckSize * PositioningScale.y);

        for (int i = 0; i < FarLook; i++)
        {
            GenerateChunck(start + addition * i);
        }

        _chunkCount = FarLook;

        RegenerateTrigger rt = Instantiate(TriggerPrefab, start + new Vector3(PositioningOffset.x, 0, TriggerDistance * PositioningScale.y + PositioningOffset.y), this.transform.rotation, this.transform);

        rt.SetAction(GenerateNormalChunck, start + addition * (FarLook));
    }

    public void GenerateNormalChunck(Vector3 start)
    {

        if (_chunkCount < LevelLength)
            GenerateChunck(start);
        if (_chunkCount == LevelLength)
            GenerateFinishChunck(start);
        if (_chunkCount > LevelLength)
            GenerateEmptyChunck(start);

        RegenerateTrigger rt = Instantiate(TriggerPrefab, start + new Vector3(PositioningOffset.x, 0, (TriggerDistance + ChunckSize * (-FarLook)) * PositioningScale.y + PositioningOffset.y ), this.transform.rotation, this.transform);

        Debug.Log($"count = {_chunkCount}");

            rt.SetAction(GenerateNormalChunck, start + new Vector3(0, 0, ChunckSize * PositioningScale.y));

        _chunkCount++;
        _currentChunck++;
    }

    private void GenerateFinishChunck(Vector3 start)
    {
        GameObject chunck = new GameObject("lastchunck");
        chunck.transform.parent = this.transform;
        chunck.transform.position = this.transform.position;

        Instantiate(FinishPrefab, start + Vector3.fwd * (ChunckSize / 2), FinishPrefab.transform.rotation, chunck.transform);

        GenerateBanks(chunck, start);

        AddNewChunck(chunck);
    }

    public void GenerateEmptyChunck(Vector3 start)
    {
        GameObject chunck = new GameObject("emptychunck");
        chunck.transform.parent = this.transform;
        chunck.transform.position = this.transform.position;

        GenerateBanks(chunck, start);

        AddNewChunck(chunck);
    }

    public void GenerateChunck(Vector3 start)
    {
        currentChunck = new char[CHUNCK_WIDTH,ChunckSize];

        for (int i = 0; i < CHUNCK_WIDTH; i++)
        {
            for (int j = 0; j < ChunckSize; j++)
            {
                currentChunck[i, j] = WAT;
            }
        }

        int currentZ = 0;

        while (currentZ < ChunckSize - 1)
        {
            Func<Vector3, int, int, int> generator = GetPieceFunc();

            currentZ += generator(start, currentZ, ChunckSize - currentZ);
            currentZ += UnityEngine.Random.Range(MinDistance, MaxDistance + 1);
        }

        GameObject newChunck = PlacePrefabs(start);

        GenerateWind(newChunck, start);

        GenerateBanks(newChunck, start);

        AddNewChunck(newChunck);

        //_counter++;

        //return start + new Vector3(0, 0, ChunckSize * PositioningScale.y);



        // wanted to do it more cleverly, but alas... this at least works
        //if (_counter < ChuncksBeforeSpecial)
        //    rt.SetAction(GenerateChunck, start + new Vector3(0, 0, ChunckSize * PositioningScale.y));
        //else
        //    rt.SetAction(GenerateSpecial, start + new Vector3(0, 0, ChunckSize * PositioningScale.y));

    }

    private void GenerateBanks(GameObject newChunck, Vector3 start)
    {
        for (int i = 0; i < (int)PositioningScale.y; i++)
        {
            GameObject LeftPref = LeftBank[UnityEngine.Random.Range(0, LeftBank.Count)];
            GameObject RightPref = RightBank[UnityEngine.Random.Range(0, RightBank.Count)];

            Instantiate(LeftPref, start + Vector3.back * ChunckSize * PositioningScale.y + Vector3.forward * i * 20 + Vector3.left * BankDistance, LeftPref.transform.rotation, newChunck.transform);
            Instantiate(RightPref, start + Vector3.back * ChunckSize * PositioningScale.y + Vector3.forward * i * 20 + Vector3.right * BankDistance, RightPref.transform.rotation, newChunck.transform);
        }
    }

    private void GenerateSpecial(Vector3 start)
    {
        _counter = 0;

        RegenerateTrigger rt = Instantiate(TriggerPrefab, start + new Vector3(PositioningOffset.x, 0, (TriggerDistance + 1 - ChunckSize) * PositioningScale.y + PositioningOffset.y), this.transform.rotation, this.transform);

        rt.SetAction(GenerateChunck, start + new Vector3(0, 0, SpecialSize * PositioningScale.y));


        GameObject chunck = new GameObject("chunck");
        chunck.transform.parent = this.transform;
        chunck.transform.position = this.transform.position;

        GameObject Kraken = KrakenObstacles[UnityEngine.Random.Range(0, KrakenObstacles.Count)].prefab;

        Instantiate(Kraken, start + Vector3.forward * (SpecialSize/2) * PositioningScale.y, this.transform.rotation, chunck.transform);

        AddNewChunck(chunck);
    }

    private void AddNewChunck(GameObject newChunck)
    {
        ChunckCache.Add(newChunck);
        if (ChunckCache.Count > MaxChunckCount)
        {
            GameObject chunck = ChunckCache[0];
            ChunckCache.RemoveAt(0);
            Destroy(chunck);
        }
    }

    private void GenerateWind(GameObject newChunck, Vector3 start)
    {

        int remainingZ = ChunckSize, currentZ = 0;

        while (remainingZ >= WindLength)
        {
            currentZ = ChunckSize - remainingZ;

            Wind newWind = Instantiate(WindPrefab, start + Vector3.forward * currentZ * PositioningScale.y, this.transform.rotation, newChunck.transform);
            int windDir = windDirChanceDistr[UnityEngine.Random.Range(0, windDirChanceDistr.Count)];
            newWind.windDirection = (Wind.Direction)windDir;
            newWind.windSpeed = UnityEngine.Random.Range(MinWindSpeed, MaxWindSpeed);

            remainingZ -= WindIntervals;
        }
    }

    private GameObject PlacePrefabs(Vector3 start)
    {
        GameObject chunck = new GameObject("chunck");
        chunck.transform.parent = this.transform;
        chunck.transform.position = this.transform.position;

        for (int x = 0; x < currentChunck.GetLength(0); x++)
        {
            for (int y = 0; y < currentChunck.GetLength(1); y++)
            {
                Vector3 diviation = new Vector3(UnityEngine.Random.Range(-PositioningDiviation.x, PositioningDiviation.x), 0, UnityEngine.Random.Range(-PositioningDiviation.y, PositioningDiviation.y));
                Vector3 offset = new Vector3(x * PositioningScale.x + PositioningOffset.x, 0, y * PositioningScale.y + PositioningOffset.y);
                Vector3 prefabPos = start + offset + diviation;

                GameObject prefab = currentChunck[x, y] switch
                {
                    ROK => RockObstacles[UnityEngine.Random.Range(0, RockObstacles.Count)].prefab,
                    WIR => WirlpoolObstacles[UnityEngine.Random.Range(0, WirlpoolObstacles.Count)].prefab,
                    KRA => KrakenObstacles[UnityEngine.Random.Range(0, KrakenObstacles.Count)].prefab,
                    _ => null
                };
                if(prefab != null)
                    Instantiate(prefab, prefabPos, this.transform.rotation, chunck.transform);

                //switch (currentChunck[x, y])
                //{
                //    case ROK:
                //        Instantiate(RockObstacles[UnityEngine.Random.Range(0, RockObstacles.Count)].prefab, prefabPos, this.transform.rotation, this.transform);
                //        break;
                //    case WIR:
                //        Instantiate(WirlpoolObstacles[UnityEngine.Random.Range(0, WirlpoolObstacles.Count)].prefab, prefabPos, this.transform.rotation, this.transform);
                //        break;
                //    case KRA:
                //        Instantiate(KrakenObstacles[UnityEngine.Random.Range(0, KrakenObstacles.Count)].prefab, prefabPos, this.transform.rotation, this.transform);
                //        break;
                //    default:
                //        break;
                //}
            }
        }

        return chunck;

        //string vizualizationLog = "";

        //for (int i = 0; i < currentChunck.GetLength(0); i++)
        //{
        //    for (int j = 0; j < currentChunck.GetLength(1); j++)
        //    {
        //        vizualizationLog += currentChunck[i, j];
        //    }
        //    vizualizationLog += "\n";
        //}

        //Debug.Log(vizualizationLog);
    }

    private Func<Vector3, int,int,int> GetPieceFunc()
    {
        int Sum = RockShare + KrackenShare + WhirlpoolShare;

        if (UnityEngine.Random.Range(0, Sum) < RockShare)
        {
            return GenerateRocks;
        }
        Sum -= RockShare;
        if (UnityEngine.Random.Range(0, Sum) < KrackenShare)
        {
            return GenerateKraken;
        }
        return GenerateWhirlpool;
    }

    private int GenerateWhirlpool(Vector3 startPos, int currentZ, int remainingSpace)
    {
        int x = UnityEngine.Random.Range(1, 4);
        currentChunck[x, currentZ] = WIR;
        return 1;
    }

    private int GenerateKraken(Vector3 startPos, int currentZ, int remainingSpace)
    {
        int version = UnityEngine.Random.Range(1, 6);

        switch(version)
        {
            case 0:
                currentChunck[CHUNCK_WIDTH / 2, currentZ] = ROK;
                currentChunck[CHUNCK_WIDTH - 1, currentZ] = KRA;
                return 1;
            case 1:
                currentChunck[CHUNCK_WIDTH / 2, currentZ] = ROK;
                currentChunck[0, currentZ] = KRA;
                return 1;
            case 2:
                int x = UnityEngine.Random.Range(0, 3);
                currentChunck[CHUNCK_WIDTH / 2 - 1 + x, currentZ] = KRA;
                currentChunck[0, currentZ] = ROK;
                currentChunck[CHUNCK_WIDTH - 1, currentZ] = ROK;
                return 1;
            case 3:
                currentChunck[0, currentZ] = ROK;
                currentChunck[1, currentZ] = ROK;
                currentChunck[CHUNCK_WIDTH / 2 + 1, currentZ] = KRA;
                return 1;
            case 4:
                currentChunck[CHUNCK_WIDTH - 1, currentZ] = ROK;
                currentChunck[CHUNCK_WIDTH - 2, currentZ] = ROK;
                currentChunck[CHUNCK_WIDTH / 2 -1, currentZ] = KRA;
                return 1;
            case 5:
                currentChunck[CHUNCK_WIDTH / 2, currentZ] = KRA;
                return 1;
        }
        return 0;
    }

    

    private int GenerateRocks(Vector3 startPos, int currentZ, int remainingSpace)
    {
        int size = UnityEngine.Random.Range(0, Mathf.Min(rockFormations.Count, remainingSpace));

        bool reverse = UnityEngine.Random.Range(0, 2) > 0;

        int type = UnityEngine.Random.Range(0, rockFormations[size].GetLength(0));

        for (int y = 0; y < rockFormations[size].GetLength(1); y++)
        {
            for(int x = 0; x < rockFormations[size].GetLength(2); x++)
            {
                int addX = reverse ? rockFormations[size].GetLength(2) - x - 1 : x;
                currentChunck[addX,currentZ + y] = rockFormations[size][type,y,x];
            }
        }

        //switch(size)
        //{
        //    case 0:
        //        GenerateRocks1(startPos, currentZ);
        //        break;
        //    case 1:
        //        GenerateRocks2(startPos, currentZ);
        //        break;
        //    case 2:
        //        GenerateRocks3(startPos, currentZ);
        //        break;
        //}
        return size + 1;
    }

    private void GenerateRocks1(Vector3 startPos, int currentZ)
    {
        Debug.Log($"GR1: width: {CHUNCK_WIDTH}, size:{ChunckSize}, currentZ:{currentZ}");
        int x = UnityEngine.Random.Range(0, CHUNCK_WIDTH - 3);
        for (int i = 0; i < CHUNCK_WIDTH; i++)
        {
            currentChunck[i, currentZ] = ROK;
        }
        currentChunck[x, currentZ] = WAT;
        currentChunck[x+1, currentZ] = WAT;
    }

    private void GenerateRocks2(Vector3 startPos, int currentZ)
    {
        Debug.Log($"GR2: width: {CHUNCK_WIDTH}, size:{ChunckSize}, currentZ:{currentZ}");
        int x = UnityEngine.Random.Range(0, CHUNCK_WIDTH - 3);
        for (int i = 0; i < CHUNCK_WIDTH; i++)
        {
            currentChunck[i, currentZ] = ROK;
            currentChunck[i, currentZ+1] = ROK;
        }
        currentChunck[x, currentZ] = WAT;
        currentChunck[x + 1, currentZ] = WAT;
        currentChunck[x, currentZ+1] = WAT;
        currentChunck[x + 1, currentZ+1] = WAT;
    }

    private void GenerateRocks3(Vector3 startPos, int currentZ)
    {
        Debug.Log($"GR3: width: {CHUNCK_WIDTH}, size:{ChunckSize}, currentZ:{currentZ}");
        int x = UnityEngine.Random.Range(0, CHUNCK_WIDTH - 3);
        for (int i = 0; i < CHUNCK_WIDTH; i++)
        {
            currentChunck[i, currentZ] = ROK;
            currentChunck[i, currentZ + 1] = ROK;
            currentChunck[i, currentZ + 2] = ROK;
        }
        currentChunck[x, currentZ] = WAT;
        currentChunck[x + 1, currentZ] = WAT;
        currentChunck[x, currentZ + 1] = WAT;
        currentChunck[x + 1, currentZ + 1] = WAT;
        currentChunck[x, currentZ + 2] = WAT;
        currentChunck[x + 1, currentZ + 2] = WAT;
    }
}
